# TP Linux 3 (Tanguy Blanco)

## I. DNS

### 1 Mise en place
* Installation du dépôt EPEL
```bash=
$ sudo yum install epel-release -y

$ sudo yum list epel-release
    Loaded plugins: fastestmirror
    Loading mirror speeds from cached hostfile
     * base: centos.mirror.fr.planethoster.net
     * epel: mirrors.ircam.fr
     * extras: distrib-coffee.ipsl.jussieu.fr
     * updates: distrib-coffee.ipsl.jussieu.fr
    Installed Packages
    epel-release.noarch                     7-13                       @epel
```
* Installation des paquets bind et bind-utils
```bash=
$ sudo yum install bind -y

$ sudo yum list bind 
    Loaded plugins: fastestmirror
    Loading mirror speeds from cached hostfile
     * base: centos.mirror.fr.planethoster.net
     * epel: mirrors.ircam.fr
     * extras: distrib-coffee.ipsl.jussieu.fr
     * updates: distrib-coffee.ipsl.jussieu.fr
    Installed Packages
    bind.x86_64                   32:9.11.4-26.P2.el7_9.3              @updates

$ sudo yum install bind-utils y
    Loaded plugins: fastestmirror
    Loading mirror speeds from cached hostfile
     * base: centos.mirror.fr.planethoster.net
     * epel: mirrors.ircam.fr
     * extras: distrib-coffee.ipsl.jussieu.fr
     * updates: distrib-coffee.ipsl.jussieu.fr
    Installed Packages
    bind-utils.x86_64             32:9.11.4-26.P2.el7_9.3              @updates
```
* Ouverture du port 53 pour les requêtes DNS
```bash=
$ sudo firewall-cmd --permanent --add-service=dns
    success   

$ sudo firewall-cmd --reload
    success

$ sudo firewall-cmd --list-all
    public (active)
      target: default
      icmp-block-inversion: no
      interfaces: ens33 ens37
      sources:
      services: dhcpv6-client dns http https ssh
      ports: 888/tcp
      protocols:
      masquerade: no
      forward-ports:
      source-ports:
      icmp-blocks:
      rich rules:
```
* Edition du fichier de conf DNS
```bash=
$ sudo vim /etc/named.conf
    //
    // named.conf
    //
    // Provided by Red Hat bind package to configure the ISC BIND named(8) DNS
    // server as a caching only nameserver (as a localhost DNS resolver only).
    //
    // See /usr/share/doc/bind*/sample/ for example named configuration files.
    //
    // See the BIND Administrator's Reference Manual (ARM) for details about the
    // configuration located in /usr/share/doc/bind-{version}/Bv9ARM.html

    options {
        listen-on port 53 { 127.0.0.1; 192.168.177.14; };
        listen-on-v6 port 53 { ::1; };
        directory 	"/var/named";
        dump-file 	"/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        recursing-file  "/var/named/data/named.recursing";
        secroots-file   "/var/named/data/named.secroots";
        allow-query     { 192.168.177.0/24; };

        /* 
         - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
         - If you are building a RECURSIVE (caching) DNS server, you need to enable 
           recursion. 
         - If your recursive DNS server has a public IP address, you MUST enable access 
           control to limit queries to your legitimate users. Failing to do so will
           cause your server to become part of large scale DNS amplification 
           attacks. Implementing BCP38 within your network would greatly
           reduce such attack surface 
        */
        recursion yes;

        dnssec-enable yes;
        dnssec-validation yes;

        /* Path to ISC DLV key */
        bindkeys-file "/etc/named.root.key";

        managed-keys-directory "/var/named/dynamic";

        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";
    };

    logging {
            channel default_debug {
                    file "data/named.run";
                    severity dynamic;
            };
    };

    zone "." IN {
        type hint;
        file "named.ca";
    };

    zone "tp2.cesi" IN {

             type master;

             file "/var/named/tp3.cesi.db";

             allow-update { none; };
    };

    zone "168.177.10.in-addr.arpa" IN {

              type master;

              file "/var/named/168.177.10.db";

              allow-update { none; };
    };

    include "/etc/named.rfc1912.zones";
    include "/etc/named.root.key";

```
* Edition du fichier tp2.cesi.db
```bash=
$ sudo vim /var/named/tp2.cesi.db
    $TTL    604800
    @   IN  SOA     ns1.tp2.cesi. root.tp2.cesi. (
                                                    1001    ;Serial
                                                    3H      ;Refresh
                                                    15M     ;Retry
                                                    1W      ;Expire
                                                    1D      ;Minimum TTL
                                                    )

    ;Name Server Information
    @      IN  NS      ns1.tp2.cesi.

    ns1 IN  A       192.168.177.14
    web     IN  A       192.168.177.10
    db     IN  A       192.168.177.11
    rp     IN  A       192.168.177.12

```
* Edition du fichier 
```bash=
$ sudo vim /var/named/168.177.10.db
    $TTL    604800
    @   IN  SOA     ns1.tp2.cesi. root.tp2.cesi. (
                                                    1001    ;Serial
                                                    3H      ;Refresh
                                                    15M     ;Retry
                                                    1W      ;Expire
                                                    1D      ;Minimum TTL
                                                    )

    ;Name Server Information
    @ IN  NS      ns1.tp2.cesi.

    14.177.168.192 IN PTR ns1.tp2.cesi.

    ;PTR Record IP address to HostName
    10      IN  PTR     web.tp2.cesi.
    11      IN  PTR     db.tp2.cesi.
    12      IN  PTR     rp.tp2.cesi.
```
* Activation et démarrage du service named
```bash= 
$ sudo systemctl enable named --now

$ sudo systemctl status named
    ● named.service - Berkeley Internet Name Domain (DNS)
       Loaded: loaded (/usr/lib/systemd/system/named.service; enabled; vendor preset: disabled)
       Active: active (running) since Fri 2020-12-18 11:52:35 CET; 20s ago
      Process: 2284 ExecStart=/usr/sbin/named -u named -c ${NAMEDCONF} $OPTIONS (code=exited, status=0/SUCCESS)
      Process: 2282 ExecStartPre=/bin/bash -c if [ ! "$DISABLE_ZONE_CHECKING" == "yes" ]; then /usr/sbin/named-checkconf -z "$NAMEDCONF"; else echo "Checking of zone files is disabled"; fi (code=exited, status=0/SUCCESS)
     Main PID: 2286 (named)
       CGroup: /system.slice/named.service
               └─2286 /usr/sbin/named -u named -c /etc/named.conf
```
### 2. Test résolution DNS
```bash=
$ sudo dig web.tp2.cesi @192.168.177.14
    ; <<>> DiG 9.11.4-P2-RedHat-9.11.4-26.P2.el7_9.3 <<>> web.tp2.cesi @192.168.177.14
    ;; global options: +cmd
    ;; Got answer:
    ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 33260
    ;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 2

    ;; OPT PSEUDOSECTION:
    ; EDNS: version: 0, flags:; udp: 4096
    ;; QUESTION SECTION:
    ;web.tp2.cesi.                  IN      A

    ;; ANSWER SECTION:
    web.tp2.cesi.           604800  IN      A       192.168.177.10

    ;; AUTHORITY SECTION:
    tp2.cesi.               604800  IN      NS      ns1.tp2.cesi.

    ;; ADDITIONAL SECTION:
    ns1.tp2.cesi.           604800  IN      A       192.168.177.14

    ;; Query time: 0 msec
    ;; SERVER: 192.168.177.14#53(192.168.177.14)
    ;; WHEN: Fri Dec 18 15:00:14 CET 2020
    ;; MSG SIZE  rcvd: 91
```
* 4. Configuration du DNS de façon permanente
```bash=
$ sudo vim /etc/resolv.conf
    # Generated by NetworkManager
    search localdomain tp2.cesi
    nameserver 192.168.177.14  
